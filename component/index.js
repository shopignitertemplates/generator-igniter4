/*
 * React Component Generator
 * This file generates a React Component.
 *
 * The function names conform to the Yeoman spec for determining execution priority.  Priority order can be referenced here:
 * @link http://yeoman.io/authoring/running-context.html
 *
 * If you need a "private" method, just place an underscore in front of the method name, e.g. GeneratorName.prototype._dontRunMe.
 *
 * @author erikharper
 */

'use strict';

 // Include Dependencies
var path = require('path'),
    yeoman = require('yeoman-generator'),
    chalk = require('chalk'),
    scriptBase = require('../script-base.js');

/*
 * Generator
 * Defines the behavior of the generator.
 */
var ReactGenerator = module.exports = yeoman.generators.Base.extend({

  constructor: function () {
    yeoman.generators.Base.apply(this, arguments);

    this.argument('name', {
      required: true,
      type: String,
      desc: 'The component name'
    });

  },

  /*
   * Prompting
   * This defines the question prompts that the generator will ask for input
   */
  prompting: function () {

    // Call this.async(). This will return a function, that you then pass into your asynchronous task as a callback (see below).
    var done = this.async();

    // Igniter Greeting
    scriptBase.igniterGreeting.call(this);

    // 1. Set up Prompts
    var prompts = [
      {
        name: 'dependencies',
        message: 'What dependencies does this React component require? \n(List them out comma separated and Capitalized for best presentation)'
      }
    ];

    // 2. Handle the Prompt answers
    this.prompt(prompts, function (props) {

      var re = /\s*,\s*/,
          dependenciesArray = props.dependencies.split(re),
          dependencies = '',
          dependencyVariables = '';

      dependenciesArray.forEach(function (element, i, array) {
        element.trim();
        if (i == (array.length - 1)) {
          dependencies += "'" + element.toLowerCase() + "'";
          dependencyVariables += element;
        } else {
          dependencies += "'" + element.toLowerCase() + "',\n\t";
          dependencyVariables += element + ", ";
        }
      });

      this.dependencies = dependencies;
      this.dependencyVariables = dependencyVariables;

      done();

    }.bind(this));
  },

  writing: function () {

    var pathToModuleFile = 'public/dev/scripts/components/' + this.name + '/';

    // Create the Module
    this.template('_component.js', pathToModuleFile + this.name + '.js');
    this.template('_component.scss', pathToModuleFile + '_' + this.name + '.scss');
  }

});
