/** @jsx React.DOM */
/**
 * <%= _.capitalize(name) %>
 * Update the Description
 *
 * @author
 */
define([
  'react',
  'fluxxor',
  <%= dependencies %>
], function(React, Fluxxor, <%= dependencyVariables %>) {
  'use strict';

  return React.createClass({
    render: function() {
      return (
        <div className='<%= name.toLowerCase() %>'><%= _.capitalize(name) %> Component</div>
      );
    }
  });

});
