'use strict';

var chalk = require('chalk');

module.exports = {

 igniterGreeting: function() {
    // Create a triangle for the SI Logo
    var t = "/\\";

    // ShopIgniter Greeting!
    this.log("\n           " + chalk.red.bgRed(t) + "         ");
    this.log("          " + chalk.red.bgRed(t+t) + "       ");
    this.log("         " + chalk.yellow.bgYellow(t) +  chalk.white.bgWhite(t) + chalk.yellow.bgYellow(t) + "      ");
    this.log("        " + chalk.yellow.bgYellow(t) + chalk.white.bgWhite(t+t) + chalk.yellow.bgYellow(t) + "     ");
    this.log("       " + chalk.yellow.bgYellow(t) + chalk.blue.bgBlue(t) + chalk.cyan.bgCyan(t) + chalk.blue.bgBlue(t) + chalk.yellow.bgYellow(t) + "    ");
    this.log("        " + chalk.blue.bgBlue(t) + chalk.blue.bgBlue(t) + chalk.blue.bgBlue(t) + chalk.blue.bgBlue(t) + "    ");
    this.log("         " + chalk.blue.bgBlue(t) + "  " + chalk.blue.bgBlue(t) + "    ");
    this.log("        Igniter     \n");
    this.log(chalk.white('You\'re using the fantastic Igniter 4 Template Generator.  \n'));
  }
}
