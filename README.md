# ShopIgniter 4.x Template Generator
> [Yeoman](http://yeoman.io) generator

## Getting Started

### Install Yeoman

Not every new computer comes with a Yeoman pre-installed. He lives in the [npm](https://npmjs.org) package repository. You only have to ask for him once, then he packs up and moves into your hard drive. *Make sure you clean up, he likes new and shiny things.*

```
$ npm install -g yo
```

### Usage

#### Install and Run
To install generator-igniter4 from npm, run:

```
$ npm install -g generator-igniter4
```

Finally, run the generator:

```
$ yo igniter4
```

The generator will scaffold out a complete template for you based on the original Theme Boilerplate.  

#### Update
No need to use git pull to update your boilerplate template!  To update your generator when changes are pushed to it, run:

```
$ npm update -g generator-igniter4
```
To apply the generator update to an existing template, simply change to the root directory of the template and run:
```
yo igniter4
```
Yeoman will automatically resolve file conflicts for you, prompting you to say yes or no when there are changes that will overwrite existing files.  This is

### Features

* Custom prompt asking for the Template Name that is used to customize the following app files:

| File          | Description |
| ------------- | ------------- |
| bower.json    | Sends in a slugified version of the template name into the bower package name field.  |
| package.json  | Does the same thing as bower.json but for package.json |
| Gruntfile.js  | Yeoman-defined 'app' and 'dist' variables are used to populate the Gruntfile to point to the correct directories. |
| README.md     | Heading uses the capitalized version of the Template Name |
| theme.yml     | theme_name uses the capitalized version of the Template Name |
|               | theme_slug uses the slugified version of the Template Name |
|               | theme_tags uses the slugified version of the Template Name |

* Direct copy of IgniterPromotionBundle/ and public/ assets
* Allows for convenient on-the-fly template creation and updating by simply running the 'yo igniter4' command with conflict resolution built-in, so there's no need to worry about overwriting files when running the generator again to pull in an update.
* Supports React JSX Compilation and boilerplate startup code.

####Javascript Module Generator
You can scaffold a fresh javascript module file that will be placed in `public/dev/scripts/modules`.  Currently only supports created AMD modules loaded with RequireJS.

#####Usage
After generating your Igniter4 Template with the `yo igniter4` command, in the root directory of your new template, run the command:
```
yo igniter4:module name-of-module
```
The generator will ask you if this module needs any dependencies, which you can specify as a comma-separated list of values.  You should list out the dependencies by name with TitleCase, e.g. `React, Fluxxor, Example` because the generator will lowercase these values for you so that they can be properly referenced in the config.js, but still retain the capitalized versions as the function arguments to the module.

####React Component Generator
You can scaffold a fresh React Component that will be placed in `public/dev/scripts/components`.  Wraps the React class inside an AMD module definition, and supports defining dependencies upon creation just like the Javascript Module Generator does.

#####Usage
After generating your Igniter4 Template with the `yo igniter4` command, in the root directory of your new template, run the command:
```
yo igniter4:component name-of-component
```
The generator will ask you if this module needs any dependencies, which you can specify as a comma-separated list of values.  You should list out the dependencies by name with TitleCase, e.g. `jQuery, Lodash, Example` because the generator will lowercase these values for you so that they can be properly referenced in the config.js, but still retain the capitalized versions as the function arguments to the module.  NOTE: the `react` and `fluxxor` dependencies will be automatically included in this generator, so you don't need to specify them when you run this command.