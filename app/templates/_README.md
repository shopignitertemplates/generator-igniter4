# <%= _.capitalize(templateName) %>

##About the App
App description goes here.

##About the Template

This template utilizes a grunt-backed workflow allows for easy
management of static assets and provides an easy to follow path for delivering a template with optimized CSS and JS,
suitable for a production environment.

###Dependencies

Before beginning ensure that you have all required system dependencies, including:

| Name          | Command                       |
| ------------- |-------------                  |
| grunt         | `npm install -g grunt-cli`    |
| sass          | `gem install sass`            |
| sass-globbing | `gem install sass-globbing`   |
| bourbon       | `gem install bourbon`         |
| neat          | `gem install neat`            |
| bitters       | `gem install bitters`         |

###Run

From the root of this directory:

1. `npm install`
2. `bower install`
3. `grunt`

###Developing

During development you can run `grunt watch` which whill automatically reload changes to CSS, JS and Twig templates.
You must be viewing the front-end using the `app-dev.php/` base for live reloading to work properly.

For speedy static template development use the `grunt serve` to spin up a lightweight, node based web server that will
serve static documents in `public/dev`.

###Building

To prepare your template for a production environment, run `grunt build`. This task will combine and optimize all CSS and
JS and place these assets in the `public/dist` directory. When accessing the front-end of the site without the
`app_dev.php/` base static JS and CSS are served from this directory. Thus, you MUST verify the site in "production mode"
before pushing changes as client instances ALWAYS run in production mode.
