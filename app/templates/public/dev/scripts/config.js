require.config({
  paths: {
    'actions': 'actions',
    'analytics': 'modules/igniter-pages-sdk',
    'constants': 'constants',
    'conversion-pixels': 'modules/conversion-pixels',
    'fluxxor': '../bower_components/fluxxor/build/fluxxor',
    'jquery': '../bower_components/jquery/dist/jquery',
    'lodash': '../bower_components/lodash/dist/lodash',
    'react': '../bower_components/react/react-with-addons',
    'stores': 'stores',
    'TimelineMax': '../bower_components/greensock/src/uncompressed/TimelineMax',
    'TweenMax': '../bower_components/greensock/src/uncompressed/TweenMax'
  },
  shim: {
    'fluxxor': {
      exports: 'fluxxor'
    }
  }
});
