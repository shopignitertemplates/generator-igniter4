/**
 * Facebook Conversion Pixels
 * Module for managing Facebook conversion pixel events.
 *
 */
define([
  'jquery'
], function($) {
  'use strict';

  // Create an empty array to store Tracked Conversion Pixels
  var trackedConversionPixels = [];

  /**
   * Conversion pixel setup.
   *
   * 1. Automagically track any pixels with the slug value "page_load"
   * 2. Setup a click handler on all anchor elements that have the data attribute
   *    "data-conversion-pixel-slug." When anchors of this type are clicked, the
   *    the location does not change to the anchor's href value until after any
   *    corresponding conversion pixels have been loaded.
   */
  var init = function () {

    // Check for defined conversion pixels
    if (window.SI && window.SI.conversionPixels) {

      // Track the page load pixel, by default.
      trackConversionPixelBySlug('page_load');

      // Set up click handlers for pixels.
      $(document).on('click', '[data-conversion-pixel-slug]', function (event) {

        // Extract the target element
        var $el = $(event.target);

        // Extract the the pixel slug value.
        var slug = $el.data('conversion-pixel-slug');

        // Track by slug.
        var pixelPromise = trackConversionPixelBySlug(slug);

        // Check if the event target is an <a> tag
        if ($el.is('a')) {

          // Grab the href attribute
          var href = $el.attr('href');

          pixelPromise.done(function () {
            // Ignore href values that are just '#'; that's bad form and can cause bad behavior.
            if (href !== '#') {

              // Proceed as normal.
              window.location = href;
            }
          });

          // Don't go anywhere, yet!
          return false;
        }

      });
    }
  };

  /**
   * Track Conversion Pixels
   * Tracks one or many conversion pixels by id. For each id passed, programmatically
   * generate and insert an img element.
   *
   * Return a promise that resolves once all pixels have been loaded.
   *
   * @param conversionPixelIds
   * @returns {*}
   */
  var trackConversionPixels = function(conversionPixelIds) {

    // Create a Deferred Object
    var deferred = $.Deferred();

    // Create empty Promises array
    var promises = [];

    // Check for Conversion Pixels
    if (conversionPixelIds && conversionPixelIds.length) {

      $.each(conversionPixelIds, function(index, conversionPixelId) {

        // If the conversion pixel has not yet been tracked, track it.
        if($.inArray(conversionPixelId, trackedConversionPixels) === -1) {

          // Make and append an image.
          var imgDeferred = $.Deferred();

          var img = $('<img/>')
            .on('load', function() {
              imgDeferred.resolve();
            })
            .attr('src', 'https://www.facebook.com/offsite_event.php?id=' + conversionPixelId )
            .attr('height', 1)
            .attr('width', 1)
            .css('display', 'none');

          $('body').append(img);

          promises.push(imgDeferred.promise());

          trackedConversionPixels.push(conversionPixelId);
        }
      });
    }

    $.when($.when.apply(null, promises)).done(function() {
      deferred.resolve();
    });

    return deferred.promise();
  };

  /**
   * Track Conversion Pixel by Slug
   * Tracks conversion pixels by conversion pixel slug value.
   *
   * Return a promise that is resolved once the pixel has been loaded.
   *
   * @param slug
   * @returns {*}
   */
  var trackConversionPixelBySlug = function (slug) {

    // Create a Deffered Object
    var deferred = $.Deferred();

    // Initialize a variable to reference a valid matched pixel
    var matchedPixel;

    // If there are no conversion pixels, bail.
    if (!window.SI || !window.SI.conversionPixels) {
      deferred.resolve();
      return deferred.promise();
    }

    // Otherwise, move along.
    for (var i in window.SI.conversionPixels) {

      // Get the Pixel
      var pixel = window.SI.conversionPixels[i];

      // By slug.
      if (pixel.slug && pixel.slug === slug && pixel.object_ids) {
        matchedPixel = pixel;
      }
    }

    if (matchedPixel) {

      // If we found the pixel, don't resolve the deferred until the pixel has been tracked.
      trackConversionPixels(matchedPixel.object_ids).done(function () {
        deferred.resolve();
      });
    } else {
      // If no matching pixel was found, resolve immediately.
      deferred.resolve();
    }

    // Return Promise
    return deferred.promise();
  };

  // Initialize
  init();

  // Return
  return {
    trackConversionPixels: trackConversionPixels,
    trackConversionPixelsBySlug: trackConversionPixelBySlug
  };

});
