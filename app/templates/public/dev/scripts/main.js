define('main', ['config'], function() {
  'use strict';
  // Optionally load stuff for the debug env.
  if(window.SI && window.SI.debug && window.SI.debug === true) {
    require(['app', 'debug'], function(app) {
      app.run();
    });
  } else {
    require(['app'], function(app) {
      app.run();
    });
  }
});
