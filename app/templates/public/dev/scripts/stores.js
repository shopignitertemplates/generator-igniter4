/**
 * Stores
 * Instantiates the Stores used in this Flux application.
 *
 * @return object - Flux store objects
 */
define([
  'components/application/application-store'
], function(ApplicationStore) {
  'use strict';

  // TODO: Write Data Manipulation Logic here (Pat's components.js file)
  // Igniter 4 Component Object
  var componentObject = {},
      asset_base_url,
      placeholder_image_path = '';

    //Placeholder filepath weather viewing from local or remote
    if (typeof (window.SI && window.SI.asset_path) !== 'undefined') {
        //Remove # sign. A bit hacky. could there be a better way?
        var asset_path = SI.asset_path.substring(0, SI.asset_path.length - 1);
        placeholder_image_path = asset_path + 'images/placeholders/';
    }else {
        placeholder_image_path = 'images/placeholders/';
    }

  // if (typeof (window.SI && window.SI.componentObject) !== 'undefined') {
  //
  //   componentObject = window.SI.componentObject;
  //   asset_base_url = window.SI.asset_base_url;
  //
  //   var images = [componentObject.carousel_image_1.asset.file_path + "." + componentObject.carousel_image_1.asset.file_extension];
  //
  //   console.log(componentObject);
  //
  // } else {

    // var images = [placeholder_image_path + "bttf1.png", placeholder_image_path + "bttf2.png", placeholder_image_path + "bttf3.png",
    //           placeholder_image_path + "bttf4.png", placeholder_image_path + "bttf5.png", placeholder_image_path + "bttf6.png"];


  // }


  return {
    ApplicationStore: new ApplicationStore.store()
  };

});
