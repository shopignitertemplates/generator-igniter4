define(['jquery'], function($) {
  'use strict';
  $.ajaxPrefilter(function(options) {
    if (options.url.indexOf('/') === 0) {
      options.url = '/app_dev.php' + options.url;
    }
  });
});