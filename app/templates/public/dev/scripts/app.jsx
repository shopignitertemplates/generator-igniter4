/**
 * Flux Application
 * Sets up the necessary parts to a Flux application.
 * Stores, Actions and the Flux Object.
 * 
 */
define([
  'react',
  'fluxxor',
  'components/application/application',
  'stores',
  'actions',
  'analytics',
  'conversion-pixels'
], function (React, Fluxxor, Application, Stores, Actions) {
  'use strict';

  // Flux
  var flux = new Fluxxor.Flux(Stores, Actions);
  window.flux = flux;

  return {
    run: function() {

      // Render the App
      React.render(<Application flux={flux} />, document.getElementById('appContainer'));

    }
  };

});
