/** @jsx React.DOM */
define([
  'react'
], function(React) {
  'use strict';

  /**
   * CSS Modal Dialog
   */
  var CssModalDialog = React.createClass({displayName: 'CssModalDialog',
    render: function() {
      return (
        React.DOM.div({className: "css-modal-dialog"}, 
          React.DOM.input({className: "css-modal-dialog__state", id: this.props.id, type: "checkbox"}), 
          React.DOM.div({className: "css-modal-dialog__window"}, 
            React.DOM.div({className: "css-modal-dialog__inner"}, 
              React.DOM.label({className: "css-modal-dialog__close", htmlFor: this.props.id}), 
              this.props.content
            )
          )
        )
      );
    }
  });

  /**
   * CSS Modal Button
   */
  var CssModalButton = React.createClass({displayName: 'CssModalButton',
    render: function() {
      return (
        React.DOM.label({className: "css-modal-button", htmlFor: this.props.htmlFor, onClick: this.props.onClick}, 
          React.DOM.div({className: "css-modal-button__title"}, this.props.title)
        )
      );
    }
  });

  return {
    Dialog: CssModalDialog,
    Button: CssModalButton
  };

});
