/**
 * CSS Modal Dialog
 * Renders a pure CSS modal dialog.
 * Does not expose a javascript API for manipulating behavior in code.
 * Contains 2 components:
 *  - CssModalDialog: UI for the Dialog itself.
 *  - CssModalButton: UI for the element to toggle the dialog.
 *
 * @author erikharper
 */
define([
  'react'
], function(React) {
  'use strict';

  /**
   * CSS Modal Dialog
   * Renders a CSS Modal Dialog window.
   *
   * @prop children - Children DOM elements to display inside the Dialog.
   */
  var CssModalDialog = React.createClass({
    render: function() {
      return (
        <div className="modal">
          <input className="modal-state" id="modal-1" type="checkbox" />
          <div className="modal-window">
            <div className="modal-inner">
              <label className="modal-close" htmlFor="modal-1"></label>
              <div className="content">
                {this.props.children}
              </div>
            </div>
          </div>
        </div>
      );
    }
  });

  /**
   * CSS Modal Button
   * Renders a "button" to trigger the CSS Modal Dialog.
   *
   * @prop string title
   */
  var CssModalButton = React.createClass({
    render: function() {
      return (
        <label htmlFor="modal-1">
          <div className="btn js-btn">{this.props.title}</div>
        </label>
      );
    }
  });

  return {
    Dialog: CssModalDialog,
    Button: CssModalButton
  };

});
