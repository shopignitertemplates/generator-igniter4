define([
  'components/application/views/mainView',
  'components/application/views/formView'
], function(MainView, FormView) {
  'use strict';

  return {
    MainView: MainView,
    FormView: FormView
  }

});
