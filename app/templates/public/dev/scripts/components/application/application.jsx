/**
 * Application
 * Top-level React Component, otherwise known as a controller-view, which renders all other React components.
 * Accepts state from external stores, rather than maintaining state itself.
 *
 */
define([
  'react',
  'fluxxor',
  'constants',
  'components/application/views',
  'jquery',
  'components/loader-accordion/loader-accordion',
  'components/css-modal/css-modal'
], function (React, Fluxxor, Constants, Views, $, LoaderAccordion, CssModal) {
  'use strict';

  // Instantiate Mixins
  var FluxMixin = Fluxxor.FluxMixin(React),
      StoreWatchMixin = Fluxxor.StoreWatchMixin;

  // React Transitions
  var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

  return React.createClass({

    // Apply the Mixins
    mixins: [FluxMixin, StoreWatchMixin("ApplicationStore")],

    getStateFromFlux: function() {

      // Get the Flux Instance
      this.flux = this.getFlux();

      return {
        application: this.flux.store("ApplicationStore").getState()
      };

    },

    componentDidMount: function() {

      // Get necessary DOM Nodes
      var $applicationLoader = $(this.refs.applicationLoader.getDOMNode()),
          $headerRegion = $(this.refs.headerRegion.getDOMNode()),
          $bodyRegion = $(this.refs.bodyRegion.getDOMNode()),
          $footerRegion = $('.footer-region');

      // Handle Position of the Application Loader
      $(window).resize(function() {
        $applicationLoader.css({
          position:'absolute',
          left: ($(window).width() - $applicationLoader.outerWidth())/2,
          top: ($(window).height() - $applicationLoader.outerHeight())/3
        });
      });

      // Call this right away to trigger loader positioning
      $(window).resize();

      // Hide All Regions initially
      $headerRegion.addClass('scaleHide');
      $bodyRegion.addClass('scaleHide');
      $footerRegion.addClass('scaleHide');

      // When the whole page loads (including images) animate everything in.
      $applicationLoader.addClass('zoomOut');

      $headerRegion.delay(300).queue(function(next){
        $(this).addClass('zoomBounceIn');
        next();
      });

      $bodyRegion.delay(500).queue(function(next){
        $(this).addClass('zoomBounceIn');
        next();
      });

      $footerRegion.delay(700).queue(function(next){
        $(this).addClass('zoomBounceIn');
        next();
      });

    },

    showView: function(view) {
      this.flux.actions.application.showView(view)
    },

    /**
     * Render
     * @return {[type]} [description]
     */
    render: function() {

      var currentView;

      switch (this.state.application.currentView) {
        case "MainView":
          currentView = <Views.MainView key="MainView"/>;
          break;
        case "FormView":
          currentView = <Views.FormView key="FormView"/>;
          break;
      }

      return (
        <section id='app'>

          <header className="header-region" ref="headerRegion">
            <button onClick={this.showView.bind(this, "MainView")}>Home</button>
            <button onClick={this.showView.bind(this, "FormView")}>Form</button>
          </header>

          <section className="body-region" ref="bodyRegion">

            <ReactCSSTransitionGroup transitionName="currentView">
              {currentView}
            </ReactCSSTransitionGroup>

          </section>

          <CssModal.Dialog>
            <h1>Terms and Conditions</h1>
            <p className="intro">Intro text lorem ipsum dolor sit ametm, quas, eaque facilis aliquid cupiditate tempora cumque ipsum accusantium illo modi commodi  minima.</p>
            <p className="body">Body text lorem ipsum dolor ipsum dolor sit sit possimus amet, consectetur adipisicing elit. Itaque, placeat, explicabo, veniam quos aperiam molestias eriam molestias molestiae suscipit ipsum enim quasi sit possimus quod atque nobis voluptas earum odit accusamus quibusdam.</p>
          </CssModal.Dialog>

          <LoaderAccordion ref="applicationLoader" />

        </section>
      );
    }

  });

});
