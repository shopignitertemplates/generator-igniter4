/**
 * Application Store
 * Manages business logic for the Application.
 *
 * @author erikharper
 */
define([
  'fluxxor',
  'constants',
  'analytics',
  'conversion-pixels'
], function(Fluxxor, Constants) {
  'use strict';

  /**
   * Actions
   * Wires up actions for this Store with the Flux Dispatcher.
   *
   * @type {Object}
   */
  var actions = {

    showView: function(view) {
      this.dispatch("showView", {view: view});
    }

  };

  /**
   * Store
   * Handles the business logic for this application.
   *
   * @type object - Flux Store
   */
  var ApplicationStore = Fluxxor.createStore({

    /**
     * Action Handlers
     * Wires up actions with their handlers.
     * @type {Object}
     */
    actions: {
      showView: "showView"
    },

    /**
     * Initialize
     * Sets default settings for the store.
     */
    initialize: function(options) {

      // This is where the starting view can be set
      this.currentView = 'MainView';

    },

    /**
     * Get State
     * Returns the current state of the application.
     *
     * @return object
     */
    getState: function() {
      return {
        currentView: this.currentView,
        mainViewContent: "This is MainView's Content",
        formViewContent: "This is FormView's Content"
      }
    },

    /**
     * Show View
     * Switches the application's current view.
     *
     * @param object payload - contains the view to switch to.
     */
    showView: function(payload) {

      // Track Analytics
      window.igniter.analytics.gaSend(Constants.ga.actions.eng, Constants.ga.types.l, 'Show ' + payload.view);

      // Show View
      this.currentView = payload.view;
      this.emit("change");
    }

  });

  return {
    store: ApplicationStore,
    actions: actions
  };

});
