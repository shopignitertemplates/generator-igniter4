/**
 * Form View
 * The Form View of the application.
 *
 */
define([
  'react',
  'fluxxor',
  'components/application/views/footerView'
], function(React, Fluxxor, Footer) {
  'use strict';

  // Instantiate Flux Child Mixin
  var FluxMixin = Fluxxor.FluxMixin(React);

  return React.createClass({

    // Apply Mixins
    mixins: [FluxMixin],

    render: function() {

      var applicationState = this.getFlux().store('ApplicationStore').getState();

      return (
        <div className='formView'>
          <div className="panel">
            {applicationState.formViewContent}
            <input type="text" placeholder="name" />
            <input type="text" placeholder="address" />
            <input type="text" placeholder="city" />
            <input type="text" placeholder="state" />
            <input type="text" placeholder="zip" />
            <input type="text" placeholder="phone" />
            <input type="text" placeholder="email" />
            <button>Submit</button>
          </div>
          <Footer />
        </div>
      );
    }
  });

});
