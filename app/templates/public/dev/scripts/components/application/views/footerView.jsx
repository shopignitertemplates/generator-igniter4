/**
 * FooterView
 * Update the Description
 *
 * @author
 */
define([
  'react',
  'fluxxor',
  'components/css-modal/css-modal'
], function(React, Fluxxor, CssModal) {
  'use strict';

  return React.createClass({
    render: function() {
      return (
        <footer className="footer-region panel" ref="footerRegion">
          <CssModal.Button title="Terms and Conditions" />
        </footer>
      );
    }
  });

});
