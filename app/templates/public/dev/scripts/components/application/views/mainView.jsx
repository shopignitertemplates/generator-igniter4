/**
 * Main View
 * The Main View of the application.
 *
 */
define([
  'react',
  'fluxxor',
  'components/application/views/footerView'
], function(React, Fluxxor, Footer) {
  'use strict';

  // Instantiate Flux Child Mixin
  var FluxMixin = Fluxxor.FluxMixin(React);

  return React.createClass({

    // Apply Mixins
    mixins: [FluxMixin],

    render: function() {

      var applicationState = this.getFlux().store('ApplicationStore').getState();

      return (
        <div className='mainView'>
          <div className="panel">
            {applicationState.mainViewContent}
            <img src="http://greatscott.jpg.to" />
          </div>
          <Footer />
        </div>
      );
    }
  });

});
