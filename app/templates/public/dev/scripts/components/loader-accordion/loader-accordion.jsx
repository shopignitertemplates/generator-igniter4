/**
 * Loader Accordion
 * Generates an accordion-style loader element used to indicate application processing.
 *
 * CSS3 Animation styling courtesy of:
 * @link http://tobiasahlin.com/spinkit/
 *
 * @prop string barColor - sets the color of the animating bars.
 * @prop string numberOfBars - sets the number of bars.
 */
define([
  'react'
], function (React) {
  'use strict';

  return React.createClass({
    render: function() {

      var style = {
            backgroundColor: this.props.barColor ? this.props.barColor : 'white'
          },
          numberOfBars = this.props.numberOfBars ? this.props.numberOfBars : 5,
          bars = {},
          barClassName = '';

      for (var i = 1; i <= numberOfBars; i++) {
        barClassName = "rect" + i;
        bars[i] = <div className={barClassName} key={i} style={style}></div>;
      }

      return (
        <div className='loader-accordion'>
          {bars}
        </div>
      );
    }
  });

});
