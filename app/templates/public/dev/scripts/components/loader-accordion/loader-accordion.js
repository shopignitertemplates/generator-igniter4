/** @jsx React.DOM */
define([
  'react'
], function (React) {
  'use strict';

  return React.createClass({
    render: function() {

      var style = {
            backgroundColor: this.props.barColor ? this.props.barColor : 'white'
          },
          numberOfBars = this.props.numberOfBars ? this.props.numberOfBars : 5,
          bars = {},
          barClassName = '';

      for (var i = 1; i <= numberOfBars; i++) {
        barClassName = "rect" + i;
        bars[i] = React.DOM.div({className: barClassName, key: i, style: style});
      }

      return (
        React.DOM.div({className: "loader-accordion"}, 
          bars
        )
      );
    }
  });

});
