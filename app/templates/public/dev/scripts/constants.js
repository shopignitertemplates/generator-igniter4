/**
 * Constants
 * Defines constants used in the Flux application.
 *
 * @return object
 */
define([], function() {
  'use strict';

  /*
   * Initialize Variable Default Settings Here
   */
  var assetBaseUrl = '',
      adCampaign = null,
      adCampaignId = '',
      componentObject = {},
      placeholderImagePath = 'images/placeholders/';

  // Check for the SI Object
  var SI = window['SI'] || {};

  // Set SI Platform Variables Here
  if (SI) {

    // Placeholder Image Path whether viewing from local or remote
    if (SI['asset_path']) {
        var asset_path = SI['asset_path'].substring(0, SI['asset_path'].length - 1);
        placeholderImagePath = asset_path + 'images/placeholders/';
    }

    // Ad Campaign Id
    adCampaign = SI['adcampaign'] || null;

    if (SI['adcampaign'] || null) {
      adCampaignId = adCampaign['campaignMedium'];
    }
  }

  /*
   * Google Analytics Constants
   */
  var ga = {
    actions: {
      cns: 'consideration_action',
      cnv: 'conversion_action',
      eng: 'engagement_action',
      psv: 'passive',
      exp: 'expand_action'
    },
    types: {
      l: 'link_click',
      o: 'other_click',
      p: 'photo_view',
      s: 'share',
      vplay: 'video_play',
      vpause: 'video_pause',
      vresume: 'video_resume',
      vend: 'passive',
      exp: 'expand'
    }
  }

  /**
   * IE Fix for window.location.origin
   */
  if (!window.location.origin) {
    window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
  }

  return {

    componentObject: SI['componentObject'] || {},
    assetBaseUrl: SI['asset_base_url'] || null,
    placeholderImagePath: placeholderImagePath,
    adCampaignId: adCampaignId,
    ga: ga

  };
});
