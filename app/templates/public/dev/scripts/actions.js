/**
 * Actions
 * Defines Actions for the Flux application.
 *
 * @return object
 */
define([
  'components/application/application-store'
], function(ApplicationStore) {
  'use strict';

  return {
    application: ApplicationStore.actions
  };

});
