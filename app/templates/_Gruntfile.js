// Generated on <%= (new Date).toISOString().split('T')[0] %> using <%= pkg.name %> <%= pkg.version %>
'use strict';

module.exports = function (grunt) {

  // show elapsed time at the end
  require('time-grunt')(grunt);

  // load all grunt tasks
  require('load-grunt-tasks')(grunt);

  /**
   * Grunt Configuration
   * Configures initial settings all Grunt Tasks.
   */
  grunt.initConfig({

    /**
     * Watch Task
     * Handles all tasks that run while watching for changes.
     *
     * @type {Object}
     */
    watch: {
      scriptCompile: {
        files: ['<%= app %>/**/*.js', '<%= app %>/**/*.jsx'],
        tasks: ['compileScriptsDev']
      },
      sass: {
      files: ['<%= app %>/styles/{,*/}*.{scss,sass}', '<%= app %>/scripts/{,*/}*.{scss,sass}', '<%= app %>/scripts/components/{,*/}*.{scss,sass}'],
        tasks: ['sass:dev', 'sass:dist', 'autoprefixer']
      },
      styles: {
        files: ['<%= app %>/styles/{,*/}*.css'],
        tasks: ['copy:styles', 'copy:fonts', 'autoprefixer']
      },
      livereload: {
        options: {
          livereload: 35829
        },
        files: [
          'IgniterPromotionBundle/**/*.twig',
          '{public/.tmp,<%= app %>}/styles/{,*/}*.css',
          '{public/.tmp,<%= app %>}/scripts/{,*/}*.js',
          '<%= app %>/scripts/{,*/}*.jsx',
          '<%= app %>/scripts/{,*/}*.js',
          '<%= app %>/scripts/{,*/}*.scss',
          '<%= app %>/styles/{,*/}*.scss',
          '<%= app %>/*.html'
        ]
      }
    },

    /**
     * AutoPrefixer Task
     *
     */
    autoprefixer: {
      options: {
        browsers: ['last 1 version']
      },
      dist: {
        files: [
          {
            expand: true,
            cwd: '.tmp/styles/',
            src: '{,*/}*.css',
            dest: '.tmp/styles/'
          }
        ]
      }
    },

    /**
     * Clean Task
     * @type {Object}
     */
    clean: {
      dist: {
        files: [
          {
            dot: true,
            src: [
              'public/.tmp',
              '<%= dist %>/*',
              '!<%= dist %>/.git*'
            ]
          }
        ]
      },
      dev: '.tmp'
    },

    /**
     * Connect Task
     * @type {Object}
     */
    connect: {
      options: {
        port: 9000,
        livereload: 35829,
        hostname: '*'
      },
      livereload: {
        options: {
          open: true,
          base: [
            'public/.tmp',
            '<%= app %>'
          ]
        }
      },
      dist: {
        options: {
          open: true,
          base: '<%= dist %>',
          livereload: false
        }
      }
    },

    /**
     * Copy Task
     * Copies files into directories based on environment and other conditions.
     */
    copy: {
      dist: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: '<%= app %>',
            dest: '<%= dist %>',
            src: [
              '*.{ico,png,txt}',
              '.htaccess',
              'images/{,*/}*.{webp,gif}',
              'styles/fonts/{,*/}*.{eot,ttf,otf,cff,afm,lwfn,ffil,fon,pfm,pfb,woff,svg,std,pro,xsf}'
            ]
          }
        ]
      },

      fonts: {
        expand: true,
        dot: true,
        cwd: '<%= app %>/styles',
        dest: 'public/.tmp/dist/styles/',
        src: [
          'fonts/{,*/}*.{eot,ttf,otf,cff,afm,lwfn,ffil,fon,pfm,pfb,woff,svg,std,pro,xsf}'
        ]
      },

      styles: {
        expand: true,
        dot: true,
        cwd: '<%= app %>/styles',
        dest: 'public/.tmp/styles/',
        src: [
          '{,*/}*.css',
          'images/{,*/}*.{webp,gif}'
        ]
      },

      // Copies all Bower-installed CSS files as SCSS to @import them with Sass.
      vendorStyles: {
        files: [
          {
            expand: true,
            cwd: '<%= app %>/bower_components',
            src: ['**/*.scss', '**/*.css', '!**/*.min.css'],
            dest: '<%= app %>/bower_components',
            filter: 'isFile',
            ext: ".scss"
          }
        ]
      }
    },

    /**
     * CSS Min Task
     *
     */
    cssmin: {
      dist: {
        files: {
          '<%= dist %>/styles/main.css': [
            'public/.tmp/dist/styles/{,*/}*.css',
            '<%= app %>/styles/{,*/}*.css'
          ]
        }
      }
    },

    /**
     * Modernizr Task
     * Custom builds modernizr.js
     *
     * @type {Object}
     */
    modernizr: {
      dist: {
        devFile: '<%= app %>/bower_components/modernizr/modernizr.js',
        outputFile: '<%= dist %>/bower_components/modernizr/modernizr.js',
        files: {
          src: [
            '<%= dist %>/scripts/{,*/}*.js',
            '<%= dist %>/styles/{,*/}*.css',
            '!<%= dist %>/scripts/vendor/*'
          ]
        },
        uglify: true
      }
    },

    /*
     * React
     * Compiles React JSX Components into javascript.
     *
     */
    react: {
      default: {
        files: [{
          expand: true,
          cwd: '<%= app %>/scripts',
          src: ['**/*.jsx'],
          dest: '<%= app %>/scripts',
          ext: '.js'
        }]
      }
    },

    /**
     * RequireJS Task
     * Compiles AMD JS modules for the app.
     *
     * @link https://github.com/jrburke/r.js/blob/master/build/example.build.js
     */
    requirejs: {
      options: {
        baseUrl: '<%= app %>' + '/scripts',
        optimize: 'uglify2',
        preserveLicenseComments: false,
        useStrict: true,
        wrap: true,
        mainConfigFile: '<%= app %>' + '/scripts/config.js',
        name: '../bower_components/almond/almond',
        include: ['main'],
        excludeShallow: [
          'debug'
        ],
        insertRequire: ['main'],
        out: '<%= dist %>' + '/scripts/main.js',
        findNestedDependencies: true
      },
      dev: {
        options: {
          optimize: 'none'
        }
      },
      dist: {
        options: {
          optimize: 'uglify2'
        }
      }
    },

    /**
     * Sass (grunt-contrib-sass)
     * Compiles Sass Files to CSS using Ruby Sass.
     *
     * NOTE: Requires the sass-globbing gem.
     * Install it with `gem install sass-globbing`
     *
     */
    sass: {
      options: {
        loadPath: '<%= app %>/bower_components',
        require: ['sass-globbing']
      },
      dev: {
        style: 'expanded',
        files: {
          'public/.tmp/styles/main.css': '<%= app %>/styles/main.scss',
          'public/.tmp/dist/styles/main.css': '<%= app %>/styles/main.scss'
        }
      },
      dist: {
        style:'compressed',
        files: {
          'public/.tmp/dist/styles/main.css': '<%= app %>/styles/main.scss'
        }
      }
    }
  });

  /**
   * Compile Scripts (Dev Mode)
   * Compiles React components, then runs the requirejs dev task.
   * $ grunt compileScriptsDev
   */
  grunt.registerTask('compileScriptsDev', function() {
    grunt.task.run([
      'react',
      'requirejs:dev'
    ]);
  });

  /**
   * Compile Scripts (Dist Mode)
   * Compiles React components, then runs the requirejs dist task.
   * $ grunt compileScriptsDist
   */
  grunt.registerTask('compileScriptsDist', function() {
    grunt.task.run([
      'react',
      'requirejs:dist'
    ]);
  });

  /**
   * Serve
   * Serves the template for localhost development.
   * $ grunt serve
   */
  grunt.registerTask('serve', function () {
    grunt.task.run([
      'clean:dev',
      'compileScriptsDev',
      'copy:styles',
      'copy:vendorStyles',
      'copy:fonts',
      'sass:dev',
      'autoprefixer',
      'connect:livereload',
      'watch'
    ]);
  });

  /**
   * Redirects to the serve task
   */
  grunt.registerTask('server', function () {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve']);
  });

  /**
   * Build
   * This task builds the template for production.
   * $ grunt build
   */
  grunt.registerTask('build', [
    'clean:dist',
    'compileScriptsDist',
    // 'copy:styles',
    'copy:vendorStyles',
    // 'copy:fonts',
    'sass:dev',
    'autoprefixer',
    'cssmin',
    'modernizr',
    'copy:dist'
  ]);

  /**
   * Default
   * This task sets Grunt's default tasks to run when no arguments are supplied.
   * $ grunt
   */
  grunt.registerTask('default', [
    'build'
  ]);
};
