/*
 * ShopIgniter 4.x Template Generator
 * This file contains the configuration and instructions for scaffolding a new ShopIgniter 4 template.
 * Every method placed on the GeneratorName.prototype will be invoked in the order it is written.
 * If you need a "private" method, just place an underscore in front of the method name, e.g. GeneratorName.prototype._dontRunMe.
 *
 * @author erikharper
 */

 'use strict';

 // Include Dependencies
var util = require('util'),
    path = require('path'),
    yeoman = require('yeoman-generator'),
    chalk = require('chalk'),
    scriptBase = require('../script-base.js');

/*
 * Generator
 * Defines the behavior of the generator.
 */
var Igniter4Generator = yeoman.generators.Base.extend({

  /*
   * Init
   * Initializes the generator
   */
  initializing: function () {

    // Store some constants for injection in the Gruntfile
    this.app = "public/dev";
    this.dist = "public/dist";

    // Store the Igniter4 Generator Package Information
    this.pkg = require('../package.json');
  },

  /*
   * Ask For
   * This defines the question prompts that the generator will ask for input
   */
  prompting: function () {

    // Call this.async(). This will return a function, that you then pass into your asynchronous task as a callback (see below).
    var done = this.async();

    // Igniter Greeting
    scriptBase.igniterGreeting.call(this);

    // 1. Set up Prompts
    var prompts = [{
      name: 'templateName',
      message: 'What do you want to call your template?'
    }];

    // 2. Handle the Prompt answers
    this.prompt(prompts, function (props) {

      this.templateName = props.templateName;

      done();

    }.bind(this));
  },

  writing: {

    /*
     * App
     * Sets up the App Boilerplate Folder Structure
     */
    app: function () {

      // Create IgniterPromotionBundle
      this.mkdir('IgniterPromotionBundle');

      // Create public
      this.mkdir('public');
    },

    /*
     * Project Files
     * Copies project files
     */
    projectfiles: function () {

      // Copy IgniterPromotionBundle
      this.directory('IgniterPromotionBundle', 'IgniterPromotionBundle');

      // Copy public
      this.directory('public', 'public');

      // Copy static files
      this.copy('.bowerrc');
      this.copy('.gitignore');

      // Instead of just performing a simple copy, this.template will run Lo-Dash through the file matched from the first parameter,
      // then create and place the compiled result at the location passed in as the second argument.
      this.template('_bower.json', 'bower.json');
      this.template('_package.json', 'package.json');
      this.template('_Gruntfile.js', 'Gruntfile.js');
      this.template('_README.md', 'README.md');
      this.template('_theme.yml', 'theme.yml');

    }

  },

  end: function () {
    if (!this.options['skip-install']) {
      this.installDependencies();
    }
  }
});

// Export the Generator for Yeoman to consume
module.exports = Igniter4Generator;
